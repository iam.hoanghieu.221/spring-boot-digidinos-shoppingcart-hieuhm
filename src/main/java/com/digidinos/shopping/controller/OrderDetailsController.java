package com.digidinos.shopping.controller;

import com.digidinos.shopping.entity.OrderDetails;
import com.digidinos.shopping.entity.Orders;
import com.digidinos.shopping.service.OrderDetailsService;
import com.digidinos.shopping.service.OrdersService;
import com.digidinos.shopping.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class OrderDetailsController {

    @Autowired
    OrderDetailsService orderDetailsService;
    @Autowired
    OrdersService ordersService;
    @Autowired
    ProductService productService;

    /***
     *  Hàm lấy về danh sách OrderDetails
     */
    @GetMapping(value = "/orderDetails")
    public List<OrderDetails> getAll() {
        return orderDetailsService.findAll();
    }

    /***
     *  Hàm lấy về OrderDetail theo Order_id và Product_id
     * @param orderId
     * @param orderDetails
     * @param productId
     * @return OrderDetail
     */
    @PostMapping("/{id}/orderDetails/{productId}")
    public OrderDetails save(@RequestBody OrderDetails orderDetails, @PathVariable("id") Long orderId, @PathVariable("productId") Long productId) {
        orderDetails.setCreatedAt(new Timestamp(new Date().getTime()));
        Orders objOrders = this.ordersService.findById(orderId).get();
        orderDetails.setId(objOrders.getId());
        orderDetails.setProductId(productId);
        orderDetails.setDeleteAt(null);
        orderDetails.setUpdatedAt(new Timestamp(new Date().getTime()));
        orderDetailsService.addOrderDetails(orderDetails);
        return orderDetails;
    }

    /***
     *  Hàm lấy về danh sách OrderDetails theo Order_Id
     * @param orderId
     * @return List<OrderDetail>
     */
    @GetMapping("/orders/orderDetail/{orderId}")
    public List<OrderDetails> getOrderDetailByOrderId(@PathVariable("orderId") Long orderId) {
        return orderDetailsService.findOrderDetailByOrderId(orderId);
    }

}
