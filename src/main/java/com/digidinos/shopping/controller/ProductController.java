package com.digidinos.shopping.controller;

import com.digidinos.shopping.entity.Products;
import com.digidinos.shopping.entity.ResponseEntity;
import com.digidinos.shopping.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ProductController {
    @Autowired
    ProductService productService;

    /***
     *  Hàn tìm kiếm nhanh
     * @param pageNumber
     * @return Page<Products>
     */
    @GetMapping("/products/search/{keySearch}/{pageNumber}")
    public Page<Products> searchAccount(@PathVariable(value = "keySearch") String keySearch, @PathVariable(value = "pageNumber") Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber-1, 10);
        return productService.searchProducts(keySearch, pageable);
    }

    /***
     *  Hàm lấy về danh sách toàn bộ Product
     * @return Page<Products>
     */
    @GetMapping("/getAllProduct")
    public Page<Products> getAllProduct() {
        Pageable pageable = PageRequest.of(0, 10, Sort.by("id"));
        return this.productService.getAllProducts(pageable);
    }

    /***
     * Hàm lấy về danh sách Product theo trang
     * @param pageNumber
     * @param sizePage
     * @return Page<Products>
     */
    @GetMapping("/products/pageNumber/{pageNumber}/{sizePage}")
    public Page<Products> getListProduct(@PathVariable(value = "pageNumber") int pageNumber, @PathVariable(value = "sizePage") int sizePage) {
        Pageable pageable = PageRequest.of(pageNumber-1, sizePage, Sort.by("id"));
        return this.productService.getAllProducts(pageable);
    }

    /***
     *  Hàm lấy về Product theo product_id
     * @param id
     * @return Optional<Products>
     */
    @GetMapping("/products/{id}")
    public Optional<Products> getProductById(@PathVariable(value = "id") Long id) {
        return this.productService.getProductById(id);
    }

    /***
     *  Hàm thực hiện thêm mới một Product
     * @param file
     * @param products
     *
     * @return ResponseEntity
     */
    @PostMapping(value = "products/save", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity save(@ModelAttribute Products products, @RequestParam MultipartFile file) {
        String message = "";
        try {
            Optional<Products> optionalProducts = productService.findProductByCoode(products.getCode());
            if (optionalProducts.isPresent()) {
                message = "Product Code: " + products.getCode() + " already exist";
                return new ResponseEntity("FAIL", message);
            } else {
                products.setImage(file.getBytes());
                productService.saveProduct(products);
                message = "Add new products successfully";
                return new ResponseEntity("OK", message);
            }
        } catch (Exception e) {
            e.printStackTrace();
            message = "Has an error : " + e.getMessage();
            return new ResponseEntity("FAIL", message);
        }
    }

    /***
     *  Hàm thực hiện cập nhật thông tin Product
     * @param file
     * @param productEdit
     * @return ResponseEntity
     */
    @PutMapping(value = "products/update", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@ModelAttribute Products productEdit, @RequestParam MultipartFile file) {
        String message = "";
        try {
            Optional<Products> originProducts = productService.getProductById(productEdit.getId());
            Optional<Products> optProducts = productService.findProductByCoode(productEdit.getCode());
            if (optProducts.isPresent() && !productEdit.getCode().equals(originProducts.get().getCode())) {
                message = "Product Code: " + productEdit.getCode() + " already exist";
                return new ResponseEntity("FAIL", message);
            } else {
                productEdit.setImage(file.getBytes());
                productService.updateProduct(productEdit);
                message = "Update products : " + productEdit.getCode() + " successfully";
                return new ResponseEntity("OK", message);
            }
        } catch (Exception e) {
            e.printStackTrace();
            message = "Has an error : " + e.getMessage();
            return new ResponseEntity("FAIL", message);
        }
    }

    /***
     *  Hàm thực hiện xóa Product
     * @param products
     * @return ResponseEntity
     */
    @PutMapping(value = "products/delete")
    public ResponseEntity delete(@RequestBody Products products) {
        String message = "";
        try {
            products.setDelete(true);
            productService.updateProduct(products);
            message = "Delete products successfully \n\r Product name: " + products.getName();
            return new ResponseEntity("OK", message);
        } catch (Exception e) {
            e.printStackTrace();
            message = "Has an error : " + e.getMessage();
            return new ResponseEntity("FAIL", message);
        }
    }

    /***
     *  Hàm thực hiện upload ảnh
     * @param file
     * @return Products
     */
    @PostMapping(value = "/products/uploadNewImage")
    public Products uploadNewImage(@RequestParam MultipartFile file) {
        try {
            Products products = new Products();
            products.setImage(file.getBytes());
            return products;
        } catch (Exception e) {
            e.printStackTrace();
            return new Products();
        }
    }

}
