package com.digidinos.shopping.controller;

import com.digidinos.shopping.config.JwtAuthenticationFilter;
import com.digidinos.shopping.config.JwtTokenProvider;
import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.CustomUserDetails;
import com.digidinos.shopping.entity.ResponseEntity;
import com.digidinos.shopping.payload.LoginRequest;
import com.digidinos.shopping.payload.LoginResponse;
import com.digidinos.shopping.payload.RandomStuff;
import com.digidinos.shopping.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.http.HttpRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class LoginController {

    @Autowired
    AccountService accountService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    /***
     *  Hàm xử lý đăng nhập tài khoản
     * @param loginRequest
     * @return LoginResponse
     */
    @PostMapping("/login")
    public LoginResponse authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        // Xác thực từ username và password.
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUserName(),
                        loginRequest.getPassword()
                )
        );

        // Nếu không xảy ra exception tức là thông tin hợp lệ
        // Set thông tin authentication vào Security Context
        SecurityContextHolder.getContext().setAuthentication(authentication);
        // Trả về jwt cho người dùng.
        String jwt = jwtTokenProvider.generateToken((CustomUserDetails) authentication.getPrincipal());
        return new LoginResponse(jwt);
    }

    /***
     *  Hàm xác thực token
     * @param token
     * @return ResponseEntity
     */
    @GetMapping("/checkToken/{token}")
    public ResponseEntity checkToken(@PathVariable String token) {
        if (StringUtils.hasText(token) && jwtTokenProvider.validateToken(token)) {
            Long userId = jwtTokenProvider.getUserIdFromJWT(token);
            Account account = accountService.findAccountById(userId).get();
            return new ResponseEntity("OK", account.getUserRole());
        }
        return new ResponseEntity("FAIL", "NOT PERMISSION");
    }
}
