package com.digidinos.shopping.controller;

import com.digidinos.shopping.config.JwtTokenProvider;
import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.ResponseEntity;
import com.digidinos.shopping.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class AccountController {

    @Autowired
    AccountService accountService;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    /***
     *  Hàm lấy thông tin tài khoản từ token
     * @param token
     * @return Account
     */
    @GetMapping("/accounts/{token}")
    public Account getAccountFromToken(@PathVariable("token") String token) {
        if (StringUtils.hasText(token) && jwtTokenProvider.validateToken(token)) {
            Long userId = jwtTokenProvider.getUserIdFromJWT(token);
            Account account = accountService.findAccountById(userId).get();
            return account;
        } else {
            return null;
        }
    }

    /***
     *  Hàm lưu thông tin tài khoản mới
     * @param account
     * @return ResponseEntity
     */
    @PostMapping(value = "/accounts/save")
    public ResponseEntity save(@RequestBody Account account) {
        String message = "";
        try {
            Optional<Account> optAccount = accountService.findAccountByUsername(account.getUserName());
            if (optAccount.isPresent()) {
                message = "User name: " + optAccount.get().getUserName() + " already exist";
                return new ResponseEntity("FAIL", message);
            } else {
                account.setEncryptedPassword(bCryptPasswordEncoder.encode(account.getEncryptedPassword()));
                this.accountService.save(account);
                message = "Add new account successfully";
                return new com.digidinos.shopping.entity.ResponseEntity("OK", message);
            }
        } catch (Exception e) {
            e.printStackTrace();
            message = "Has an error : " + e.getMessage();
            return new com.digidinos.shopping.entity.ResponseEntity("FAIL", message);
        }
    }

    /***
     *  Hàm cập nhật thông tin tài khoản
     * @param accountEdit
     * @return ResponseEntity
     */
    @PutMapping("/accounts/update")
    public ResponseEntity update(@RequestBody Account accountEdit) {
        String message = "";
        try {
            Optional<Account> accountOrigin = accountService.findAccountById(accountEdit.getId());
            Optional<Account> optAccount = accountService.findAccountByUsername(accountEdit.getUserName());
            if (optAccount.isPresent() && !accountEdit.getUserName().equals(accountOrigin.get().getUserName())) {
                message = "User name: " + accountEdit.getUserName() + " already exist";
                return new ResponseEntity("FAIL", message);
            } else {
                if (accountEdit.getEncryptedPassword().equals(accountOrigin.get().getEncryptedPassword())) {
                    accountEdit.setEncryptedPassword(accountOrigin.get().getEncryptedPassword());
                } else {
                    accountEdit.setEncryptedPassword(bCryptPasswordEncoder.encode(accountEdit.getEncryptedPassword()));
                }
                this.accountService.save(accountEdit);
                message = "Update account: " + accountEdit.getUserName() + " successfully";
                return new com.digidinos.shopping.entity.ResponseEntity("OK", message);
            }
        } catch (Exception e) {
            e.printStackTrace();
            message = "Has an error : " + e.getMessage();
            return new com.digidinos.shopping.entity.ResponseEntity("FAIL", message);
        }
    }

    /***
     *  Hàm xóa thông tin tài khoản
     * @param account
     * @return ResponseEntity
     */
    @PutMapping("/accounts/delete")
    public ResponseEntity delete(@RequestBody Account account) {
        String message = "";
        try {
            this.accountService.udpate(account);
            message = "Delete account successfully";
            return new com.digidinos.shopping.entity.ResponseEntity("OK", message);
        } catch (Exception e) {
            e.printStackTrace();
            message = "Has an error : " + e.getMessage();
            return new com.digidinos.shopping.entity.ResponseEntity("FAIL", message);
        }
    }

    /***
     *  Hàn tìm kiếm nhanh
     * @param pageNumber
     * @return Page<Account>
     */
    @GetMapping("/accounts/search/{keySearch}/{pageNumber}")
    public Page<Account> searchAccount(@PathVariable(value = "keySearch") String keySearch, @PathVariable(value = "pageNumber") Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber - 1, 10);
        return accountService.searchAccounts(keySearch, pageable);
    }

    /***
     *  Hàm lấy về danh sách tài khoản
     * @param pageNumber
     * @return Page<Account>
     */
    @GetMapping("/accounts/pageNumber/{pageNumber}")
    public Page<Account> getAllAccounts(@PathVariable(value = "pageNumber") int pageNumber) {
        if (pageNumber == 1) {
            pageNumber = 0;
        } else if (pageNumber > 1) {
            pageNumber--;
        } else {
            pageNumber = 0;
        }
        Pageable pageable = PageRequest.of(pageNumber, 10, Sort.by("id"));
        return this.accountService.getAllAccount(pageable);
    }

    /***
     *  Hàm thực hiện upload ảnh
     * @param file
     * @return Account
     */
    @PostMapping(value = "/account/uploadNewImage")
    public Account uploadNewImage(@RequestParam MultipartFile file) {
        try {
            Account account = new Account();
            account.setImage(file.getBytes());
            return account;
        } catch (Exception e) {
            e.printStackTrace();
            return new Account();
        }
    }
}