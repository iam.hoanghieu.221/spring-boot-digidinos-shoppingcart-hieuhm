package com.digidinos.shopping.controller;


import com.digidinos.shopping.entity.OrderDetails;
import com.digidinos.shopping.entity.Orders;
import com.digidinos.shopping.entity.ResponseEntity;
import com.digidinos.shopping.service.OrderDetailsService;
import com.digidinos.shopping.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class OrdersController {

    @Autowired
    OrdersService ordersService;

    @Autowired
    OrderDetailsService orderDetailsService;

    /***
     *  Hàm lấy về danh sách Order
     * @param pageNumber
     * @return Page<Orders>
     */
    @GetMapping("/orders/{pageNumber}")
    public Page<Orders> getAll(@PathVariable(value = "pageNumber") Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber-1, 10, Sort.by("id"));
        return ordersService.findAll(pageable);
    }

    /***
     *  Hàm thực hiện tìm kiếm nhanh
     */
    @GetMapping("/orders/search/{keySearch}/{pageNumber}")
    public Page<Orders> searchOrder(@PathVariable(value = "keySearch") String keysearch, @PathVariable(value = "pageNumber") Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber-1, 10);
        return ordersService.searchOrders(keysearch, pageable);
    }

    /***
     *  Hàm này thực hiện lưu một Order mới
     * @param obj
     * @return ResponseEntity
     */
    @PostMapping("/orders/save")
    public ResponseEntity save(@RequestBody Orders obj) {
        String message = "";
        try {
            ordersService.addOrders(obj);
            List<OrderDetails> lsOrderDetail = obj.getLsOrderDetailsByOrderId();
            for (OrderDetails item : lsOrderDetail) {
                item.setOrderId(obj.getId());
                orderDetailsService.addOrderDetails(item);
            }
            message = "Order successfully";
            return new ResponseEntity("OK", message);
        } catch (Exception e) {
            e.printStackTrace();
            message = "Has an error : " + e.getMessage();
            return new ResponseEntity("Fail", message);
        }
    }

    /***
     *  Hàm thực hiện xóa Order
     * @param obj
     * @return ResponseEntity
     */
    @PutMapping("/orders/delete")
    public ResponseEntity delete(@RequestBody Orders obj) {
        String message = "";
        try {
            ordersService.delete(obj);
            List<OrderDetails> lsOrderDetail = obj.getLsOrderDetailsByOrderId();
            if (lsOrderDetail.size() > 0) {
                for (OrderDetails item : lsOrderDetail) {
                    item.setOrderId(obj.getId());
                    orderDetailsService.delete(item);
                }
            }
            message = "Order delete successfully";
            return new ResponseEntity("OK", message);
        } catch (Exception e) {
            e.printStackTrace();
            message = "Has an error : " + e.getMessage();
            return new ResponseEntity("Fail", message);
        }
    }

}
