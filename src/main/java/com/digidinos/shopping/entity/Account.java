package com.digidinos.shopping.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "accounts")
public class Account implements Serializable {
    @Id
    @Column(name = "ID", nullable = false,length = 5)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "USER_NAME",nullable = true,length = 20)
    private String userName;

    @Lob
    @Column(name = "IMAGE", columnDefinition="LONGBLOB")
    private byte[] image;

    @Column(name = "ACTIVE",nullable = true)
    private boolean active;

    @Column(name="ENCRYPTED_PASSWORD",nullable = true, length = 128)
    private String encryptedPassword;

    @Column(name="USER_ROLE",nullable = true, length = 20)
    private String userRole;

    @Column(name="IS_DELETE")
    private boolean isDelete;

    @Column(name="DELETED_AT")
    private Timestamp deleteAt;

    @Column(name="CREATED_AT")
    private Timestamp createAt;

    @Column(name="UPDATED_AT")
    private Timestamp updateAt;




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public Timestamp getDeleteAt() {
        return deleteAt;
    }

    public void setDeleteAt(Timestamp deleteAt) {
        this.deleteAt = deleteAt;
    }

    public Timestamp getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Timestamp createAt) {
        this.createAt = createAt;
    }

    public Timestamp getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Timestamp updateAt) {
        this.updateAt = updateAt;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
