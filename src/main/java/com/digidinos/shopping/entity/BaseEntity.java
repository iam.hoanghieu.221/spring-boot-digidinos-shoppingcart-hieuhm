package com.digidinos.shopping.entity;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

public class BaseEntity implements Serializable{

    private Long id;
    private Date createAt;
    private Date updateAt;
    private Date deleteAt;
    private boolean isDeleted;
}
