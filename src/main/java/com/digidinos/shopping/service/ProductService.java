package com.digidinos.shopping.service;

import com.digidinos.shopping.entity.Products;
import com.digidinos.shopping.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    private Logger logger = LoggerFactory.getLogger(ProductService.class);

    /***
     *  Hàm lấy về danh sách toàn bộ {@link Products}
     * @param pageable
     * @return Page<Products>
     */
    public Page<Products> getAllProducts(Pageable pageable) {
        Page<Products> pageList = null;
        try {
            pageList = productRepository.findAllByDeleteFalse(pageable);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return pageList;
    }

    /***
     *  Hàm thêm mới một {@link Products}
     * @param products
     */
    public void saveProduct(Products products) {
        try {
            products.setDelete(false);
            products.setDeletedAt(null);
            products.setCreatedAt(new Timestamp(new Date().getTime()));
            products.setUpdatedAt(new Timestamp(new Date().getTime()));
            productRepository.save(products);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    /***
     *  Hàm lấy cập nhật {@link Products}
     * @param products
     */
    public void updateProduct(Products products) {
        try {
            Products editProduct;
            editProduct = this.getProductById(products.getId()).get();
            products.setDeletedAt(null);
            products.setCreatedAt(editProduct.getCreatedAt());
            products.setUpdatedAt(new Timestamp(new Date().getTime()));
            productRepository.save(products);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    /***
     *  Hàm lấy về một {@link Products} theo id
     * @param id
     * @return Optional<Products>
     */
    public Optional<Products> getProductById(Long id) {
        return productRepository.findById(id);
    }

    public Optional<Products> findProductByCoode(String keyCodeProduct) {
        return productRepository.findProductsByCode(keyCodeProduct);
    }

    /***
     *  Hàm tìm kiếm {@link Products}
     * @param keySearch
     * @param pageable
     * @return Page<Products>
     */
    public Page<Products> searchProducts(String keySearch, Pageable pageable) {
        Page<Products> pageList = null;
        try {
            pageList = productRepository.searchProducts(keySearch, pageable);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pageList;
    }

}
