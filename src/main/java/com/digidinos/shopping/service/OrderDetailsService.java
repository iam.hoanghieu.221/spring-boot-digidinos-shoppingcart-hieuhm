package com.digidinos.shopping.service;

import com.digidinos.shopping.entity.OrderDetails;
import com.digidinos.shopping.repository.OrderDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class OrderDetailsService {

    @Autowired
    OrderDetailsRepository orderDetailsRepository;

    /***
     *  Hàm tìm kiếm OrderDetails
     * @return List<OrderDetails>
     */
    public List<OrderDetails> findAll() {
        return orderDetailsRepository.findAll();
    }

    /***
     *  Hàm thêm mới một OrderDetails
     * @param orderDetails
     */
    public void addOrderDetails(OrderDetails orderDetails) {
        try {
            orderDetails.setDeleteAt(null);
            orderDetails.setCreatedAt(new Timestamp(new Date().getTime()));
            orderDetails.setUpdatedAt(new Timestamp(new Date().getTime()));
            this.orderDetailsRepository.save(orderDetails);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     *  Hàm xóa một OrderDetails
     * @param orderDetails
     */
    public void delete(OrderDetails orderDetails) {
        try {
            orderDetails.setDelete(true);
            orderDetails.setDeleteAt(new Timestamp(new Date().getTime()));
            orderDetails.setUpdatedAt(new Timestamp(new Date().getTime()));
            this.orderDetailsRepository.save(orderDetails);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     *  Hàm lấy về danh sách {@link OrderDetails} theo orderId
     * @param orderId
     * @return List<OrderDetails>
     */
    public List<OrderDetails> findOrderDetailByOrderId(Long orderId) {
        try {
            return this.orderDetailsRepository.findOrderDetailsByOrderId(orderId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
