package com.digidinos.shopping.service;

import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.CustomUserDetails;
import com.digidinos.shopping.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService implements UserDetailsService {
    @Autowired
    AccountRepository accountRepository;

    /***
     *  Hàm lấy về thông tin UserDetails theo User name
     * @param username
     * @return UserDetails
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository.findByUserName(username).get();
        if (account == null) {
            throw new UsernameNotFoundException(username);
        }
        return new CustomUserDetails(account);
    }

    /***
     *  Hàm lấy về thông tin UserDetails theo User_id
     * @param id
     * @return UserDetails
     */
    public UserDetails loadUserById(Long id) throws Exception {
        Account account = accountRepository.findById(id).get();
        if (account == null) {
            throw new Exception("Not found user_id : " + id);
        }
        return new CustomUserDetails(account);
    }

    /***
     *  Hàm lấy về thông tin Account theo account_id
     * @param accountId
     * @return Optional<Account>
     */
    public Optional<Account> findAccountById(Long accountId) {
        return this.accountRepository.findById(accountId);
    }

    /***
     *  Hàm lấy về thông tin Account theo User name
     * @param username
     * @return Optional<Account>
     */
    public Optional<Account> findAccountByUsername(String username) {
        return this.accountRepository.findByUserName(username);
    }

    /***
     *  Hàm lấy về danh sách toàn bộ Account
     * @param pageable
     * @return Page<Account>
     */
    public Page<Account> getAllAccount(Pageable pageable) {
        Page<Account> pageList = null;
        try {
            pageList = this.accountRepository.getAllAccount(pageable);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pageList;
    }

    /***
     *  Hàm lấy về danh sách toàn bộ Account
     * @return List<Account>
     */
    public List<Account> getListAccount() {
        return this.accountRepository.findAll();
    }

    /***
     *  Hàm lấy thêm mới một Account
     * @param account
     */
    public void save(Account account) {
        try {
            account.setDelete(false);
            account.setDeleteAt(null);
            account.setCreateAt(new Timestamp(new Date().getTime()));
            account.setUpdateAt(new Timestamp(new Date().getTime()));
            this.accountRepository.save(account);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     *  Hàm lấy cập nhật một Account
     * @param account
     */
    public void udpate(Account account) {
        try {
            account.setDeleteAt(null);
            account.setUpdateAt(new Timestamp(new Date().getTime()));
            this.accountRepository.save(account);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     *  Hàm tìm kiếm Account
     * @param keySearch
     * @param pageable
     * @return Page<Account>
     */
    public Page<Account> searchAccounts(String keySearch, Pageable pageable) {
        Page<Account> pageList = null;
        try {
            pageList = accountRepository.searchAccounts(keySearch, pageable);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pageList;
    }


}