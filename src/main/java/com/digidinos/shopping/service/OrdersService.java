package com.digidinos.shopping.service;

import com.digidinos.shopping.entity.Orders;
import com.digidinos.shopping.repository.OrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

@Service
public class OrdersService {

    @Autowired
    OrdersRepository ordersRepository;

    /***
     *  Hàm lấy về danh sách toàn bộ {@link Orders}
     * @param pageable
     * @return Page<Orders>
     */
    public Page<Orders> findAll(Pageable pageable) {
        Page<Orders> pageList = null;
        try {
            pageList = ordersRepository.getAllByDeleteFalse(pageable);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pageList;
    }

    /***
     *  Hàm tìm kiếm {@link Orders}
     * @param pageable
     * @param keySearch
     * @return Page<Orders>
     */
    public Page<Orders> searchOrders(String keySearch, Pageable pageable) {
        Page<Orders> pageList = null;
        try {
            pageList = ordersRepository.searchOrders(keySearch, pageable);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pageList;
    }

    /***
     *  Hàm lấy về một {@link Orders} theo orderId
     * @param id
     * @return Optional<Orders>
     */
    public Optional<Orders> findById(Long id) {
        return ordersRepository.findById(id);
    }

    /***
     *  Hàm thêm mới một {@link Orders}
     * @param orders
     */
    public void addOrders(Orders orders) {
        try {
            orders.setDeletedAt(null);
            orders.setOrderDate(new Timestamp(new Date().getTime()));
            orders.setCreatedAt(new Timestamp(new Date().getTime()));
            orders.setUpdatedAt(new Timestamp(new Date().getTime()));
            this.ordersRepository.save(orders);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     *  Hàm cập nhật một {@link Orders}
     * @param orders
     */
    public void delete(Orders orders) {
        try {
            orders.setDelete(true);
            orders.setDeletedAt(new Timestamp(new Date().getTime()));
            orders.setUpdatedAt(new Timestamp(new Date().getTime()));
            this.ordersRepository.save(orders);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
