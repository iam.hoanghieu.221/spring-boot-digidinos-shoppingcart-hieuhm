package com.digidinos.shopping.repository;

import com.digidinos.shopping.entity.OrderDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailsRepository extends BaseRepository<OrderDetails,Long> {

    /***
     *  Hàm lấy về danh sách Order detail theo Order_ID
     * @param orderId
     * @return Page<OrderDetails>
     */
    @Query("select  u from OrderDetails u where u.orderId = :orderId")
    List<OrderDetails> findOrderDetailsByOrderId(@Param("orderId") Long orderId);

}
