package com.digidinos.shopping.repository;

import com.digidinos.shopping.entity.Orders;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdersRepository extends BaseRepository<Orders, Long> {

    /***
     *  Hàm tìm kiếm Orders
     * @param keySearch
     * @param pageable
     * @return Page<Orders>
     */
    @Query("select u from Orders u where " +
            " u.customerName like  %:keySearch% or " +
            " u.customerPhone like  %:keySearch% or " +
            " u.customerEmail like  %:keySearch% or " +
            " u.customerAddress like  %:keySearch% ")
    Page<Orders> searchOrders(@Param("keySearch") String keySearch, Pageable pageable);

    /***
     *  Hàm lấy về danh sách toàn bộ Orders
     * @param pageable
     * @return Page<Orders>
     */
    @Query("select u from Orders u where u.isDelete = false")
    Page<Orders> getAllByDeleteFalse(Pageable pageable);

}
