package com.digidinos.shopping.repository;

import com.digidinos.shopping.entity.Products;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends BaseRepository<Products,Long> {

    /***
     *  Hàm tìm kiếm Products
     * @param keySearch
     * @param pageable
     * @return Page<Products>
     */
    @Query("select u from Products u where " +
            " u.code like  %:keySearch% or " +
            " u.name like  %:keySearch% ")
    Page<Products> searchProducts(@Param("keySearch") String keySearch, Pageable pageable);

    /***
     *  Hàm lấy về danh sách toàn bộ Products
     * @param pageable
     * @return Page<Products>
     */
    @Query("SELECT u from Products u where u.isDelete = 0")
    Page<Products> findAllByDeleteFalse(Pageable pageable);

    /***
     *  Hàm lấy về danh sách Products theo Product_code
     * @param keyCodeProduct
     * @return Optional<Products>
     */
    @Query("SELECT u from Products u where u.code = :keyCodeProduct")
    Optional<Products> findProductsByCode(@Param("keyCodeProduct") String keyCodeProduct);
}
