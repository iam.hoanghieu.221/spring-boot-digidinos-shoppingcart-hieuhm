package com.digidinos.shopping.repository;

import com.digidinos.shopping.entity.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository  extends BaseRepository<Account, Long> {

    /***
     *  Hàm thực hiện tìm kiếm account
     * @param keySearch
     * @param pageable
     * @return Page<Account>
     */
    @Query("select u from Account u where " +
            " u.userName like  %:keySearch% or " +
            " u.userRole like  %:keySearch%")
    Page<Account> searchAccounts(@Param("keySearch") String keySearch, Pageable pageable);

    /***
     *  Hàm lấy về danh sách toàn bộ account
     * @param pageable
     * @return Page<Account>
     */
    @Query("select u from Account u where u.isDelete = false")
    Page<Account> getAllAccount(Pageable pageable);

    /***
     *  Hàm tìm kiếm Account theo User name
     * @param keyUsername
     * @return Page<Account>
     */
    @Query("select u from Account u where u.userName like :keyUsername")
    Optional<Account> findByUserName(@Param("keyUsername") String keyUsername);

}
