package com.digidinos.shopping;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.OrderDetails;
import com.digidinos.shopping.entity.Orders;
import com.digidinos.shopping.entity.Products;
import com.digidinos.shopping.repository.OrderDetailsRepository;
import com.digidinos.shopping.service.AccountService;
import com.digidinos.shopping.service.OrdersService;
import com.digidinos.shopping.service.ProductService;
import com.digidinos.shopping.service.StorageService;
import com.github.javafaker.Faker;

@Component
public class ShoppingcartCommandRunner implements CommandLineRunner {
	protected final Log logger = LogFactory.getLog(getClass());

	@Resource
	StorageService storageService;
	@Autowired
	OrdersService orderService;
	@Autowired
	ProductService productService;
	@Autowired
	AccountService accountService;
	@Autowired
	OrderDetailsRepository orderDetailsRepository;

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public void run(String... args) throws Exception {
		String strArgs = Arrays.stream(args).collect(Collectors.joining("|"));
		logger.info("Application started with arguments:" + strArgs);

//		if (strArgs.equals("migrate")) {
		migratedData();
//		}
	}

	protected void migratedData() {
		storageService.deleteAll();
		storageService.init();

		Faker faker = new Faker();
		Orders order = null;
		Products product = null;
		Date date = new Date();
		long time = date.getTime();

		Account account = new Account();
		account.setActive(true);
		account.setCreateAt(new Timestamp(time));
		String password = bCryptPasswordEncoder.encode("123456");// 123456
		account.setEncryptedPassword(password);
		account.setUserName("admin");
		account.setUserRole("ADMIN");
		accountService.save(account);

//		for (int i = 0; i < 50; i++) {
//			order = new Orders();
//			order.setAmount(faker.number().numberBetween(10, 1000));
//			order.setCreatedAt(new Timestamp(time));
//			order.setCustomerAddress(faker.address().fullAddress());
//			order.setCustomerEmail(faker.internet().emailAddress());
//			order.setCustomerName(faker.name().name());
//			order.setCustomerPhone(faker.phoneNumber().cellPhone());
//			order.setOrderDate(new Timestamp(time));
//			order.setDelete(false);
//			order.setOrderNum(faker.number().numberBetween(100, 200));
//			orderService.addOrders(order);
//
//			product = new Products();
//			// random image https://i.picsum.photos/id/1000/255/255.jpg
//			URL urlInput;
//			try {
//				int ranId = faker.number().numberBetween(1, 1000);
//				String imgUrl = String.format("https://i.picsum.photos/id/%d/255/255.jpg", ranId);
//				urlInput = new URL(imgUrl);
//				BufferedImage urlImage = ImageIO.read(urlInput);
//				ByteArrayOutputStream baos = new ByteArrayOutputStream();
//				ImageIO.write(urlImage, "jpg", baos );
//				baos.flush();
//				byte[] imageInByte = baos.toByteArray();
//				baos.close();
//				product.setImage(imageInByte);
//			} catch (MalformedURLException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			product.setCode(faker.regexify("[a-z1-9]{10}"));
//			product.setName(faker.name().fullName());
//			product.setPrice(faker.number().numberBetween(100, 1000));
//			product.setCreatedAt(new Timestamp(time));
//			product.setDelete(false);
//			productService.saveProduct(product);
//
//			OrderDetails orderDetail = new OrderDetails();
//			orderDetail.setAmount(faker.number().numberBetween(100, 1000));
//			orderDetail.setPrice(faker.number().randomNumber());
//			orderDetail.setQuantity(faker.number().numberBetween(10, 100));
//			orderDetail.setDelete(false);
//			orderDetail.setProductId(product.getId());
//			orderDetail.setOrderId(order.getId());
//			orderDetail.setCreatedAt(new Timestamp(time));
//			orderDetailsRepository.save(orderDetail);
//		}
	}
}
